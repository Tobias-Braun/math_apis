package Gauss;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args0){
		
		boolean running = true;
		Scanner s = new Scanner(System.in);
		System.out.println("Format your lin input the following way:");
		System.out.println("<line>;<line> ... and in a line: <char> <char> ");
		System.out.println("Example: 5 4 3 2;1 2 3 4;");
		while (running){
			System.out.println("type in your lin to compute real numbers, 'mod' to compute integers mod n");
			String input = s.nextLine();
			if (input != "mod"){
				solvedoubles(input);
			}
		}
		}
	
	private static void solveint(Scanner s, int mod){
		int[][] lin;
		try {
			String input = s.nextLine();
			String[] lines = input.split(";");
			int rows = lines.length;
			String[] columnexample = lines[0].split(" ");
			int columns = columnexample.length;
			lin = new int[rows][columns];
			for (int line = 0; line<rows; line++){
				String[] row = lines[line].split(" ");
				for(int col = 0; col<columns; col++){
					lin[line][col] = Integer.valueOf(row[col]);
				}
			}
			Rechner R = new Rechner(lin);
			//R.spin();
			R.show();
			System.out.println("solving lin ... ");
			R.solve();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Array initialisation failed - maybe you typed in a wrong format?");
		}
	}
	
	private static void solvedoubles(String input){
		double[][] lin;
		try {
			String[] lines = input.split(";");
			int rows = lines.length;
			String[] columnexample = lines[0].split(" ");
			int columns = columnexample.length;
			lin = new double[rows][columns];
			for (int line = 0; line<rows; line++){
				String[] row = lines[line].split(" ");
				for(int col = 0; col<columns; col++){
					lin[line][col] = Double.valueOf(row[col]);
				}
			}
			Rechner R = new Rechner(lin);
			//R.spin();
			R.show();
			R.solve();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Array initialisation failed - maybe you typed in a wrong format?");
		}
	}
}
