package Gauss;

public class Rechner {
	
	public double[][] dlin;
	public int[][] ilin;
	
	Rechner(double[][] arr){
		this.dlin = arr; 
	}
	Rechner(int[][] arr){
		this.ilin = arr;
	}
	/*public void solve(int mod){
		
		for (int i = 0; i<ilin[0].length-2;i++){
			if (ilin[i][i] == 0){
				boolean changed = false;
				for (int j = 0;j<ilin.length && !changed; j++){
					if (ilin[j][i]% mod != 0){
						
						for (int k= 0; k < ilin[0].length; k++){
							ilin[i][k] = (ilin[i][k] + ilin[j][k]) % mod;
						}
						changed = true;
					}
				}
				//System.out.println("changed matrix");
				//show();
			}
		}
		
		for (int i = 0; i < ilin.length; i++){
			//System.out.println("#newlayer");
			for (int k = i+1; k<ilin.length; k++){
				if (ilin[k][i] != 0 && ilin[i][i] != 0){
					double tochange = ilin[k][i];
					for(int j = i; j<ilin[k].length; j++){
						//System.out.println(dlin[k][j] + " minus " + dlin[i][j]/dlin[i][i]*tochange);
						ilin[k][j] = ilin[k][j] - ilin[i][j]/ilin[i][i]*tochange;
					}
					//System.out.println("Changed Matrix");
				} //else System.out.println("Didnt change Matrix");
				//show();
			}
		}
		//System.out.println("+++Reverse+++");
		for (int i = ilin[0].length-2;i>0; i--){
			ilin[i][ilin[i].length-1] /= ilin[i][i];
			ilin[i][i] = 1;
			for (int j = i-1;j >= 0; j--){
				double temp = ilin[j][i];
				ilin[j][i] = 0;
				ilin[j][ilin[j].length-1] -= temp*ilin[i][ilin[i].length-1];
			}
		}
		for (int i = 0; i<ilin[0].length-1; i++){
			float result = ilin[i][ilin[0].length-1];
			System.out.println("x"+(i+1) + " = " + result);
		}
	}*/
	
	public void solve(){
		//show();
		//preparing matrix in case there are zeros where there should be the solution
		
		for (int i = 0; i<dlin[0].length-2;i++){
			if (i < dlin.length && i <dlin[i].length && dlin[i][i] == 0){
				boolean changed = false;
				for (int j = 0;j<dlin.length && !changed; j++){
					if (dlin[j][i] != 0){
						
						for (int k= 0; k < dlin[0].length; k++){
							dlin[i][k] = dlin[i][k] + dlin[j][k];
						}
						changed = true;
					}
				}
				//System.out.println("changed matrix");
				//show();
			}
		}
		
		
		for (int i = 0; i < dlin.length; i++){
			//System.out.println("#newlayer");
			for (int k = i+1; k<dlin.length; k++){
				if (dlin[k][i] != 0 && dlin[i][i] != 0){
					double tochange = dlin[k][i];
					for(int j = i; j<dlin[k].length; j++){
						//System.out.println(dlin[k][j] + " minus " + dlin[i][j]/dlin[i][i]*tochange);
						dlin[k][j] = dlin[k][j] - dlin[i][j]/dlin[i][i]*tochange;
					}
					//System.out.println("Changed Matrix");
				} //else System.out.println("Didnt change Matrix");
				show();
			}
		}
		System.out.println("+++Reverse+++");
		for (int i = dlin[0].length-2;i>0; i--){
			dlin[i][dlin[i].length-1] /= dlin[i][i];
			dlin[i][i] = 1;
			for (int j = i-1;j >= 0; j--){
				double temp = dlin[j][i];
				dlin[j][i] = 0;
				dlin[j][dlin[j].length-1] -= temp*dlin[i][dlin[i].length-1];
			}
			show();
		}
		//Output of results:
		show();
		for (int i = 0; i<dlin[0].length-1; i++){
			float result = (float)(dlin[i][dlin[0].length-1]/dlin[i][i]);
			System.out.println("x"+(i+1) + " = " + result);
		}
	}
	public void show(){
		for (double[] zeile : dlin){
			for (double real : zeile){
				System.out.printf(" %s", real);
			}
			System.out.println("");
		}
		System.out.println("/////////////////////////");
	}
	public void spin(){
		show();
		double[][] temp = new double[dlin[0].length][dlin.length];
		for (int i = 0; i < dlin.length; i++){
			for (int j = 0; j < dlin[i].length; j++){
				temp[i][j] = dlin[j][i];
			}
		}
		dlin = temp;
		show();
		System.out.println("++++++++++++++++++++++++++++++++++++++++");
		System.out.println("");
	}
}
